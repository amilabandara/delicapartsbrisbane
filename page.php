<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<div class="wrapper" id="page-wrapper">

<section id="main-content-wrapper" class="content">
	<div id="main-content-inner-wrapper" class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="vehicle-menu">
						<h3 class="menu-title">Mitsubishi Delica</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(151);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-8"><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div>
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					 
					<div class="vehicle-menu">
						<h3 class="menu-title">Nissan Elgrand</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(152);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-8"><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div>
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					<?php get_sidebar( 'left' ); ?></div>
			<div class="col-md-8">
				

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'page' ); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

		
			</div>
			<div class="col-md-2"><?php get_sidebar( 'right' ); ?></div>
		</div>
	</div>
</section>

</div><!-- Wrapper end -->

<?php get_footer(); ?>
