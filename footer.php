<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package understrap
 */

$the_theme = wp_get_theme();
$container = get_theme_mod( 'understrap_container_type' );
?>

<?php //get_sidebar( 'footerfull' ); ?>
<div class="wrapper footer-outer">

	<div id="wrapper-footer" class="container">

		<div class="row">

			<div class="col-md-12 text-center">

				<footer class="site-footer" id="colophon">

					<div class="site-info">
						&copy <?php echo Date('Y');?> <b><a rel="home" href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></b> All Right Reserved.
					</div><!-- .site-info -->

				</footer><!-- #colophon -->

			</div><!--col end -->

		</div><!-- row end -->

	</div><!-- container end -->

</div><!-- wrapper end -->

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

</body>

</html>

