<?php
/**
 * Template Name: Two Column
 */
get_header();

$container   = get_theme_mod( 'understrap_container_type' );
$sidebar_pos = get_theme_mod( 'understrap_sidebar_position' );

?>

<section class="page-container">
	<div class="container" id="page-wrapper">

	

		<div class="row">

			<!-- Do the left sidebar check -->
			<div class="col-md-3">
				<ul class="verticle-filters list-group">
					<li class="list-group-item verticle-filter-header">
						<h3 class="verticle-filter-header">Technical Information</h3>
					</li>
					<li class="list-group-item">
						<a href="index.php/tips-for-installing-auto-transmission/">Tips for installing auto transmission</a>
					</li>
					<li class="list-group-item">
						<a href="index.php/location-of-speed-sensor-and-speedo-sensor/">Location of speed sensor and speedo sensor</a>
					</li>
				</ul>
				<br>
				<ul class="verticle-filters list-group">
					<li class="list-group-item verticle-filter-header">
						<h3 class="verticle-filter-header">General Information</h3>
					</li>
					<li class="list-group-item">
						<a href="index.php/tos">Terms and conditions of use</a>
					</li>
					<li class="list-group-item">
						<a href="index.php/secure-payments-with-delica-parts-brisbane/">Secure payment</a>
					</li>
					<li class="list-group-item">
						<a href="index.php/shipments-and-returns/">Delivery Services</a>
					</li>
					<li class="list-group-item">
						<a href="index.php/privacy-policy/">Privacy Policy</a>
					</li>
				</ul>
			</div>
			<div class="col-md-9">
				<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'loop-templates/content', 'page' ); ?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->
			</div>
			

		</div><!-- #primary -->

	

	

</div><!-- Container end -->
</section>


</div><!-- Wrapper end -->

<?php get_footer(); ?>
