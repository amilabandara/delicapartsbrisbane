<?php
function understrap_remove_scripts() {
    wp_dequeue_style( 'understrap-styles' );
    wp_deregister_style( 'understrap-styles' );

    wp_dequeue_script( 'understrap-scripts' );
    wp_deregister_script( 'understrap-scripts' );

    // Removes the parent themes stylesheet and scripts from inc/enqueue.php
}
add_action( 'wp_enqueue_scripts', 'understrap_remove_scripts', 20 );

add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {

	// Get the theme data
	$the_theme = wp_get_theme();

    wp_enqueue_style( 'child-understrap-styles', get_stylesheet_directory_uri() . '/css/child-theme.min.css', array(), $the_theme->get( 'Version' ) );
    wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'popper-scripts', get_template_directory_uri() . '/js/popper.min.js', array(), false);
    wp_enqueue_script( 'child-understrap-scripts', get_stylesheet_directory_uri() . '/js/child-theme.min.js', array(), $the_theme->get( 'Version' ), true );
	wp_enqueue_script( 'jquery-ui', '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js');
}

if( function_exists('acf_add_options_page') ) {
 
    $option_page = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
 
}

/**
 * Ensure cart contents update when products are added to the cart via AJAX
 */
function my_header_add_to_cart_fragment( $fragments ) {
 
    ob_start();
    $count = WC()->cart->cart_contents_count;
    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>"><?php
    if ( $count > 0 ) {
        ?>
        <span class="cart-contents-total"><?php echo WC()->cart->get_cart_total(); ?></span>
        <span class="cart-contents-count"><?php echo esc_html( $count ); ?></span>
        <?php            
    }
        ?></a><?php
 
    $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;
}
add_filter( 'woocommerce_add_to_cart_fragments', 'my_header_add_to_cart_fragment' );

// Change number or products per row to 3
add_filter('loop_shop_columns', 'loop_columns');
if (!function_exists('loop_columns')) {
    function loop_columns() {
        return 3; // 3 products per row
    }
}

// define the woocommerce_before_shop_loop_item_title callback 
function action_woocommerce_before_shop_loop_item_title(  ) { 
    $html='<div class="row">
                        <div class="col-12"><span class="product-name">'.the_title().'</span></div>
                    </div>';

    return $html;
}; 

add_action( 'admin_menu', 'clear_unwanted' );
function clear_unwanted() {
    remove_menu_page('tvr-microthemer.php');   
}

function clear_admin() {
    
    global $wp_admin_bar;
    
  
    $wp_admin_bar->remove_menu('wp-mcr-shortcut');
    
}

add_filter( 'woocommerce_subcats_from_parentcat_by_ID', 'woocommerce_subcats_from_parentcat_by_ID' );
function woocommerce_subcats_from_parentcat_by_ID( $arg = array() ) {

    $class="";
    $subcats = get_categories(array('hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $arg['parent_id'],
       'taxonomy' => 'product_cat'));

    //$html =  '<ul class="verticle-filters list-group">';
         foreach ($subcats as $sc) {
                if($sc->slug==$arg['selected_slug']){
                    $class = 'active';
                }
                else{
                     $class="";
                }
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                $html .= '<li class="list-group-item '.$class.'"><a class="" href="'. $link .'"><b>'.$sc->name.'</b><br><small>'.$sc->description.'</small></a></li>';
               
            }
       // $html .= '</ul>';

        echo $html;
}


add_filter( 'shop_horizontal_filters', 'shop_horizontal_filters' );
function shop_horizontal_filters( $arg = array() ) {

    $class="inactive-filter";
    $subcats = get_categories(array('hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $arg['parent_id'],
       'taxonomy' => 'product_cat'));

    $html =  '<ul class="horizontal-filters list-unstyled list-inline">';
       // if($arg['parent_id']!='151' && $arg['parent_id']!='152'){
            $all = get_term_by('id',$arg['parent_id'],'product_cat');

            if($all->slug==$arg['selected_slug'] || $arg['selected_slug']=='' ){
                    $class = 'active-filter';
            }
            else{
                $class="inactive-filter";
            }
            $html .= '<li class="list-inline-item"><a class="btn '.$class.'" href="'. get_term_link($all->slug,'product_cat') .'">All</a></li>';
       // }
        
            foreach ($subcats as $sc) {
                if($sc->slug==$arg['selected_slug']){
                    $class = 'active-filter';
                }
                else{
                     $class="inactive-filter";
                }
                $link = get_term_link( $sc->slug, $sc->taxonomy );
                $html .= '<li class="list-inline-item"><a class="btn '.$class.'" href="'. $link .'">'.$sc->name.'</a></li>';
               
            }
        $html .= '</ul>';

        echo $html;
}

function wp_42573_fix_template_caching( WP_Screen $current_screen ) {
    // Only flush the file cache with each request to post list table, edit post screen, or theme editor.
    if ( ! in_array( $current_screen->base, array( 'post', 'edit', 'theme-editor' ), true ) ) {
        return;
    }
    $theme = wp_get_theme();
    if ( ! $theme ) {
        return;
    }
    $cache_hash = md5( $theme->get_theme_root() . '/' . $theme->get_stylesheet() );
    $label = sanitize_key( 'files_' . $cache_hash . '-' . $theme->get( 'Version' ) );
    $transient_key = substr( $label, 0, 29 ) . md5( $label );
    delete_transient( $transient_key );
}
add_action( 'current_screen', 'wp_42573_fix_template_caching' );

//unregister existing widgets
add_action( 'after_setup_theme', 'remove_default_sidebars', 11 );
function remove_default_sidebars(){
    remove_action( 'widgets_init', 'understrap_widgets_init' );
}

//Registering new widget areas

function delica_widgets_init() {
    register_sidebar( array(
        'name'          => __( 'Right Sidebar', 'delicapartsbrisbane' ),
        'id'            => 'right-sidebar',
        'description'   => 'Right sidebar widget area',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );

    register_sidebar( array(
        'name'          => __( 'Left Sidebar', 'delicapartsbrisbane' ),
        'id'            => 'left-sidebar',
        'description'   => 'Left sidebar widget area',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
}

add_action( 'widgets_init', 'delica_widgets_init' );

function dpb_get_subcategories($parent_id){
    $subcats = get_categories(array('hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $parent_id,
       'taxonomy' => 'product_cat'));

    return $subcats;
}

function dpb_get_sitemap(){
    $mds = dpb_get_subcategories(151);
    $negs = dpb_get_subcategories(152);
    $parts = dpb_get_subcategories(153);
    $part_types = dpb_get_subcategories(154);

    $html .='<div id="site-map">
                    <div class="row">
                        <div class="col">
                           
                            <ul class="tree">


    <li>Shop
     <ul>
      <li>Mitsubishi Delica
        <ul>';
    foreach($mds as $md){
           $html .= '<li>'.$md->name.'<ul>';
               foreach($parts as $part){
                    $html .='<li>'.$part->name.'<ul>';
                            foreach($part_types as $part_type){
                                $html .='<li>'.$part_type->name.'</li>';
                    } 
                       $html .='</ul></li>';
                        } 
                $html .='</ul> </li>';
           
            } 
        $html .='</ul>
      </li>
      <li>Nissan
        <ul>';
            foreach($negs as $negs){
            $html .='<li>'.$negs->name.'
                <ul>';
                    foreach($parts as $part){
                    $html .='<li>'.$part->name.'
                        <ul>';
                            foreach($part_types as $part_type){
                                $html.='<li>'.$part_type->name.'</li>';
                        }
                        $html .='</ul>
                    </li>';
                    }
                $html .='</ul>
            </li>';
            } 
        $html .='</ul>
      </li>
     </ul>
    </li>
   </ul>
                        </div>
                    </div>
                </div>';

    return $html;
}

add_shortcode( 'dpb_sitemap', 'dpb_get_sitemap' );

if ( ! function_exists( 'woocommerce_breadcrumb' ) ) {

    /**
     * Output the WooCommerce Breadcrumb.
     *
     * @param array $args Arguments.
     */
    function woocommerce_breadcrumb( $args = array() ) {
    	global $post;
    	if(Is_product()){
    		$wp_session = WP_Session::get_instance();
    		$model_slug = $wp_session['model'];
    		$model =  get_term_by('slug', $model_slug, 'product_cat');
			$model_name = $model->name;
			

			$type_slug = $wp_session['type'];
			$type = get_term_by('slug', $type_slug, 'product_cat');
			$type_name = $type->name;
    	}
    	else{
    		if(trim(esc_sql($_GET['model']))==''){
    			$model = get_queried_object();
				$model_name = $model->name;
				
    		}
    		else if(trim(esc_sql($_GET['model']))!=''){
    			$model_slug = trim(esc_sql($_GET['model']));
    			$model =  get_term_by('slug', $model_slug, 'product_cat');
    			$model_name = $model->name;

    			$type = get_queried_object();
    			$type_name = $type->name;
    		}
    	}

    	$model_link = get_category_link($model->term_id);
    	$type_link = get_category_link($type->term_id);
    	$parent_id = get_ancestors($model->term_id, 'product_cat');
    	if(!empty($parent_id)){
    		$parent = get_term_by('id', $parent_id[0], 'product_cat');
    		$parent_name = $parent->name;
    		$parent_link = get_category_link($parent_id[0]);
    	}
      ob_start();
       
       ?>
       <style type="text/css">
       		.custom-breadcrumb{
       			background-color: #000000;
       			font-size: 12px;
			    font-weight: bold;
			    line-height: 40px;
       		}

       		.custom-breadcrumb .container{
       			background-color: #ffffff
       		}
       </style>
	<section class="custom-breadcrumb">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<?php echo  $parent_name!='' ? '<a href="'.$parent_link.'">'.$parent_name.' ></a>' : ''; ?>
					<a href="<?php echo $model_link; ?>"><?php echo $model_name ?></a>
					<?php echo  $type!='' ? '<a href="'.$type_link.'"> > '.$type_name.'</a>' : ''; ?>
					<?php if(Is_product()): ?>
					<a href="<?php echo get_the_permalink();?>">> <?php echo get_the_title(); ?></a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</section>
      <?php echo ob_get_clean();
    }
}

// add_action( 'init', 'jk_remove_wc_breadcrumbs' );
function jk_remove_wc_breadcrumbs() {
    remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
}