<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

	<?php
		/**
		 * woocommerce_before_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
		 * @hooked woocommerce_breadcrumb - 20
		 * @hooked WC_Structured_Data::generate_website_data() - 30
		 */
		do_action( 'woocommerce_before_main_content' );

		global $wp_query;
		if(woocommerce_page_title(false)=='Shop'){
			$cat = null;
		}
		else{
			$cat = $wp_query->get_queried_object();

			$wp_session = WP_Session::get_instance();

			if(!empty($_GET['model'])){
			$wp_session['model'] = trim(esc_sql($_GET['model']));
			}
			if(!empty($_GET['type'])){
			$wp_session['type'] = trim(esc_sql($_GET['type']));
			}
			if(!empty($_GET['condition'])){
			$wp_session['condition'] = trim(esc_sql($_GET['condition']));
			}
		

        if($cat->parent > 0){
        	switch ($cat->parent) {
        	case 151:$wp_session['model'] = $cat->slug;
        			$wp_session['type'] ='';
        			$wp_session['condition'] ='';
        		break;
        	case 152 :$wp_session['model'] = $cat->slug;
        			$wp_session['type'] ='';
        			$wp_session['condition'] ='';
        		break;
        	case 153 :$wp_session['type'] = $cat->slug;
        		break;
        	case 154:
        		$wp_session['condition'] = $cat->slug;
        		break;
        }
        }
        else{
        	switch ($cat->term_id) {
        		case 153:
        			$wp_session['type'] = $cat->slug;
        			break;
        		
        		case 154:
        			$wp_session['condition'] = $cat->slug;
        			break;
        	}
        }


		}
		
        

    if(is_product_category()){
    ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            jQuery('#mega-menu-item-133').addClass('mega-current-menu-item');
        });
    </script>

<?php } ?>

<section id="main-content-wrapper" class="content">
	<div id="main-content-inner-wrapper" class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="vehicle-menu">
						<h3 class="menu-title">Mitsubishi Delica</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(151);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="<?php echo get_home_url();?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="<?php echo get_home_url();?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					 
					<div class="vehicle-menu">
						<h3 class="menu-title">Nissan Elgrand</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(152);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="<?php echo get_home_url();?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="<?php echo get_home_url();?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					<?php get_sidebar( 'left' ); ?></div>
			<div class="col-md-8">
				<!--  -->
				<div class="row">
		
		<div class="col">
			<div class="row">
				<div class="col-md-12">
					<header class="woocommerce-products-header">

		<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

			<!-- <h1 class="woocommerce-products-header__title page-title">Shop</h1> -->

		<?php endif; ?>

		<?php
			/**
			 * woocommerce_archive_description hook.
			 *
			 * @hooked woocommerce_taxonomy_archive_description - 10
			 * @hooked woocommerce_product_archive_description - 10
			 */
			//do_action( 'woocommerce_archive_description' );
		?>

    </header>
				</div>
				
			</div>
		<?php
	$orderby_value = isset( $_GET['orderby'] ) ? wc_clean( $_GET['orderby'] ) : apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby' ) );
	switch ($orderby_value) {
		case 'popularity':
			$orderby = 'total_sales';
			$order='desc';
			break;
		case 'date':
			$orderby = 'date';
			$order='desc';
			break;
		case 'rating':
			$meta_key  = '_wc_average_rating';
			$orderby = 'meta_value_num';
			$order='asc';
			break;
		case 'price':
			$meta_key  = '_price';
			$orderby  ='meta_value_num';
			$order ='asc';
			break;
		case 'price-desc':
			$meta_key  = '_price';
			$orderby  ='meta_value_num';
			$order ='desc';
			break;
		default:
			$orderby = '';
			$order='';
			break;
	}
	
    $args = array(
		'post_type' => 'product',
		'meta_key'  => $meta_key,
		'orderby'   => $orderby,
		'order' =>$order,
		'paged' => get_query_var( 'paged' ),
		'tax_query' => array()
	);

	$args['tax_query'] = array('relation' => 'AND',);

	if(!empty($wp_session['model'])){
		$args['tax_query'][]=  array(
	            'taxonomy' => 'product_cat',
	            'terms' => $wp_session['model'],
	            'field' => 'slug',
	            'operator' => "IN",
	    );
	}

	if(!empty($wp_session['condition'])){
		$args['tax_query'][]=  array(
	            'taxonomy' => 'product_cat',
	            'terms' => $wp_session['condition'],
	            'field' => 'slug',
	            'operator' => "IN",
	    );
	}

	if(!empty($wp_session['type'])){
		$args['tax_query'][]=  array(
	            'taxonomy' => 'product_cat',
	            'terms' => $wp_session['type'],
	            'field' => 'slug',
	            'operator' => "IN",
	    );
	}
	 $image_term = get_term_by('slug',$wp_session['model'],'product_cat');
	 $thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

    // get the image URL
    $image = wp_get_attachment_url( $thumbnail_id ); 

    // print the IMG HTML
    ?>
		
		<?php 


		global $wp_query;
		$wp_query = new WP_Query( $args );
		// echo $GLOBALS['wp_query']->request;
		?>			

		<?php if ( $wp_query->have_posts() ) : ?>
			<div class="row">
				<div class="col-md-12">
					<?php
				/**
				 * woocommerce_before_shop_loop hook.
				 *
				 * @hooked wc_print_notices - 10
				 * @hooked woocommerce_result_count - 20
				 * @hooked woocommerce_catalog_ordering - 30
				 */
				do_action( 'woocommerce_before_shop_loop' );
			?>
				</div>
			
			</div>
			<?php woocommerce_product_loop_start(); ?>

				<?php woocommerce_product_subcategories(); ?>

				<?php while ( $wp_query->have_posts() ) : $wp_query->the_post(); ?>

					<?php
						/**
						 * woocommerce_shop_loop hook.
						 *
						 * @hooked WC_Structured_Data::generate_product_data() - 10
						 */
						do_action( 'woocommerce_shop_loop' );
					?>

					<?php wc_get_template_part( 'content', 'product' ); ?>

				<?php endwhile; // end of the loop. ?>
			<?php wp_reset_postdata(); ?>
			<?php woocommerce_product_loop_end(); ?>

			<?php
				/**
				 * woocommerce_after_shop_loop hook.
				 *
				 * @hooked woocommerce_pagination - 10
				 */
				do_action( 'woocommerce_after_shop_loop' );
			?>

		<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

			<?php
				/**
				 * woocommerce_no_products_found hook.
				 *
				 * @hooked wc_no_products_found - 10
				 */
				do_action( 'woocommerce_no_products_found' );
			?>

		<?php endif; ?>
		</div>
	</div>
				<!--  -->
			</div>
			<div class="col-md-2"><?php get_sidebar( 'right' ); ?></div>
		</div>
	</div>
</section>


	
	<?php
		/**
		 * woocommerce_after_main_content hook.
		 *
		 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
		 */
		do_action( 'woocommerce_after_main_content' );
	?>

<?php get_footer( 'shop' ); ?>
