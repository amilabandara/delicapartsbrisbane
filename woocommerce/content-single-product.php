<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<section id="main-content-wrapper" class="content">
	<div id="main-content-inner-wrapper" class="container">
		<div class="row">
			<div class="col-md-2">
				<div class="vehicle-menu">
						<h3 class="menu-title">Mitsubishi Delica</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(151);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="<?php echo get_home_url(); ?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="<?php echo get_home_url(); ?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					 
					<div class="vehicle-menu">
						<h3 class="menu-title">Nissan Elgrand</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(152);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="<?php echo get_home_url(); ?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="<?php echo get_home_url(); ?>/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
				<?php get_sidebar( 'left' ); ?></div>
			<div class="col-md-8">
				<?php
	/**
	 * woocommerce_before_single_product hook.
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>


	<!-- <div class="col-md-3"> -->
		<?php //echo do_shortcode('[woof sid="auto_shortcode" autohide=0]'); ?>
	<!-- </div> -->
	<div class="col-md-12">
		<div id="product-wrapper product-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php
		/**
		 * woocommerce_before_single_product_summary hook.
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
	?>

	<div class="summary entry-summary">

		<?php
			/**
			 * woocommerce_single_product_summary hook.
			 *
			 * @hooked woocommerce_template_single_title - 5
			 * @hooked woocommerce_template_single_rating - 10
			 * @hooked woocommerce_template_single_price - 10
			 * @hooked woocommerce_template_single_excerpt - 20
			 * @hooked woocommerce_template_single_add_to_cart - 30
			 * @hooked woocommerce_template_single_meta - 40
			 * @hooked woocommerce_template_single_sharing - 50
			 * @hooked WC_Structured_Data::generate_product_data() - 60
			 */
			do_action( 'woocommerce_single_product_summary' );
		?>

	</div><!-- .summary -->

	<?php
		/**
		 * woocommerce_after_single_product_summary hook.
		 *
		 * @hooked woocommerce_output_product_data_tabs - 10
		 * @hooked woocommerce_upsell_display - 15
		 * @hooked woocommerce_output_related_products - 20
		 */
		do_action( 'woocommerce_after_single_product_summary' );
	?>

</div><!-- #product-<?php the_ID(); ?> -->

<?php do_action( 'woocommerce_after_single_product' ); ?>
	</div>
			</div>
			<div class="col-md-2"><?php get_sidebar( 'right' ); ?></div>
		</div>
	</div>
</section>