<?php
/**
 * The template for displaying related product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;


// Ensure visibility
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
<div class="col-md-3">
	<?php
	/**
	 * woocommerce_before_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_open - 10
	 */
	//do_action( 'woocommerce_before_shop_loop_item' ); ?>

	<div class="item-wrapper regular_block">
		<div class="item-wrapper-inner">
			<div class="card">
  <img class="card-img-top" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'single-post-thumbnail' )[0]?>" alt="">
  <div class="card-body">
   		<div class="row">
				<div class="col-12 text-center"><a href="<?php echo get_permalink(); ?>"><span class="product-name"><?php echo get_the_title()?></span></a></div>
		</div>
		<div class="row">
				<div class="col-12 text-center"><span class="category-name"><?php echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?></span></div>
			</div>
			<div class="row">
				<div class="col-6 d-flex align-items-center"><span class="product-price"><?php echo $product->get_price_html(); ?></span></div>
				<div class="col-6 text-right">
					<?php if($product->is_in_stock()){ $out_of_stock =''; ?>
					<a rel="nofollow" href="/DelicaBrisbane/?add-to-cart=<?php echo get_the_ID(); ?>" data-quantity="1" data-product_id="<?php echo get_the_ID();?>" data-product_sku="" class="add_to_cart_button ajax_add_to_cart"><span class="fa-stack fa-custom-1 shopping-cart-icon">
						    <i class="fa fa-circle fa-stack-2x"></i>
						    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
						</span></a>
						<?php } else{ $out_of_stock = 'Out of Stock'; ?>
								<p>
<span class="fa-stack fa-custom-1 shopping-cart-icon icon-disabled">
								    <i class="fa fa-circle fa-stack-2x"></i>
								    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
								</span></p>
	
						<?php } ?>
				</div>
			</div>
  </div>
</div>

	  		
			
			
			
		</div>
    </div>

	<?php

	/**
	 * woocommerce_before_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_show_product_loop_sale_flash - 10
	 * @hooked woocommerce_template_loop_product_thumbnail - 10
	 */
	//thumbnail
	//do_action( 'woocommerce_before_shop_loop_item_title' );

	/**
	 * woocommerce_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_product_title - 10
	 */
	//product name
	//do_action( 'woocommerce_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item_title hook.
	 *
	 * @hooked woocommerce_template_loop_rating - 5
	 * @hooked woocommerce_template_loop_price - 10
	 */
	//product price
	//do_action( 'woocommerce_after_shop_loop_item_title' );

	/**
	 * woocommerce_after_shop_loop_item hook.
	 *
	 * @hooked woocommerce_template_loop_product_link_close - 5
	 * @hooked woocommerce_template_loop_add_to_cart - 10
	 */
	//add to cart
	//do_action( 'woocommerce_after_shop_loop_item' );
	?>
</div>
