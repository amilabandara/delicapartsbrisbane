<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="page-container">
	<div class="container">
		<div class="row">
			<div class="col-md-9 mx-auto text-center">
				<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<header class="page-header">
				<h1 class="page-title"><?php _e( '404 Not Found', 'understrap' ); ?></h1>
			</header>

			<div class="page-wrapper">
				<div class="page-content">
					<h2><?php _e( 'Maybe try a search?', 'understrap' ); ?></h2>
					
					<?php echo do_shortcode( '[wcas-search-form]' ); ?>
				</div><!-- .page-content -->
			</div><!-- .page-wrapper -->

		</div><!-- #content -->
	</div><!-- #primary -->
			</div>
		</div>
	</div>
	</section>

<?php get_footer(); ?>