<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package understrap
 */

get_header();
?>
<section id="main-content-wrapper" class="content">
	<div id="main-content-inner-wrapper" class="container" style="background-color: #fff;">
		<div class="row">
			<div class="col-md-2">
				
					
<div class="vehicle-menu">
						<h3 class="menu-title">Mitsubishi Delica</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(151);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
					 
					<div class="vehicle-menu">
						<h3 class="menu-title">Nissan Elgrand</h3>
						<div class="panel-group" id="accordion1">
						<?php 
							$delica_categories = dpb_get_subcategories(152);

							foreach($delica_categories as $dc){
						?>
						  <div class="panel panel-default">
						    <div class="panel-heading panel-first">
						     
						      	<?php
						      	$image='';
						      	$image_term = get_term_by('id',$dc->term_id,'product_cat');
						      	if($image_term){
						      		$thumbnail_id = get_woocommerce_term_meta( $image_term->term_id, 'thumbnail_id', true ); 

								    // get the image URL
								 if($thumbnail_id){
								 	$image = wp_get_attachment_url($thumbnail_id); 
								 }
								
						      	}
	 							

						      	 ?>
						      	<a href="<?php echo get_term_link( $dc->slug, $dc->taxonomy );?>"><img class="img-fluid" src="<?php echo $image; ?>" alt=""></a>

						      	 <h4 class="panel-title">

						      	<a data-toggle="collapse" data-parent="#accordion1" href="#collapse<?php echo $dc->term_id ?>">
						        <div class="row">
						      	 		<div class="col-10"><?php echo $dc->name; ?></div>
						      	 		<div class="col-2"><i class="fa fa-chevron-down pull-right"></i></div>
						      	 </div>
						      	</a>
						  	</h4>
						    </div>
						    <div id="collapse<?php echo $dc->term_id ?>" class="panel-inner panel-collapse collapse">
						      <div class="panel-body">

						        
							<?php
								$part_type = dpb_get_subcategories(153);
								foreach($part_type as $pt){
							 ?>
						        <div class="panel-group" id="accordion<?php echo $dc->term_id.$pt->term_id ?>">
						          <div class="panel panel-default">
						            <div class="panel-heading panel-second">
									<div class="row">
										<div class="col-12">
											<h4 class="panel-title">
						               <div class="row">
						      	 		<div class="col-12"><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>"><?php echo $pt->name; ?></a></div>
						      	 		<!-- <div class="col-4"><a data-toggle="collapse" data-parent="#accordion<?php echo $dc->term_id.$pt->term_id ?>" href="#collapse<?php echo $dc->term_id.$pt->term_id ?>"><i class="fa fa-chevron-down pull-right"></i></a></div> -->
						      	 </div>
						              </h4>
										</div>
									</div>
						              

						            </div>
						            <div id="collapse<?php echo $dc->term_id.$pt->term_id ?>" class="panel-collapse collapse in panel-last">
						              <div class="panel-body">
						                <ul>
						                	<?php
						                		$part_condition = dpb_get_subcategories(154);
						                		foreach($part_condition as $pc){
						                	 ?>
						                	<li><a href="index.php/product-category/type/<?php echo $pt->slug;?>?model=<?php echo $dc->slug?>&condition=<?php echo $pc->slug;?>"><?php echo $pc->name; ?></a></li>
						                	<?php } ?>
						                </ul>
						              </div>
						            </div>
						          </div>
						        </div>
							<?php } ?>


						      </div>
						    </div>
						  </div>
						<?php } ?>
						</div>
					</div>
				
				<?php get_sidebar( 'left' ); ?>
				
			</div>
			<div class="col-md-8">
				<div class="row">
					<div class="col">
						<div class="slider-wrapper">
							<?php putRevSlider('main-slider', 'homepage'); ?>
						</div>
						
					</div>
				</div>
				
				<div id="featured_products" class="section">
					<!-- <div class="row"> -->
				<div class="row">
					<div class="col text-center">
							<?php $featured_products = get_field('featured_products'); ?>
							<h2 class="section-title"><?php echo $featured_products['title']  ?></h2>
						</div>
				</div>		
				<div class="row">
				 <?php
					$args = array(
		            'post_type' => 'product',
		            'posts_per_page' => 8,
		            'tax_query' => array(
		                    array(
		                        'taxonomy' => 'product_visibility',
		                        'field'    => 'name',
		                        'terms'    => 'featured',
		                    ),
		                ),
		            );

					

					$loop = new WP_Query( $args );
		        if ( $loop->have_posts() ) {
		            while ( $loop->have_posts() ) : $loop->the_post();
					global $product;
		             ?>
              <div class="col-md-3 col-sm-6">
              	<div class="item-wrapper regular_block">
					<div class="item-wrapper-inner">
						<div class="card">
						  <img class="card-img-top" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' )[0]?>" alt="">
						  <div class="card-body">
						   <div class="row">
						<div class="col-12 text-center"><a href="<?php echo get_permalink(); ?>"><span class="product-name"><?php echo strlen(get_the_title()) <=25 ?  get_the_title() : substr(get_the_title(),0,25).'...' ?></span></a></div>
					</div>
					<div class="row">
						<div class="col-12 text-center"><span class="category-name">
							<?php //echo wc_get_product_category_list( $product->get_id(), ', ', '<span class="posted_in">' . _n( '', '', count( $product->get_category_ids() ), 'woocommerce' ) . ' ', '</span>' ); ?>
								<?php echo strlen($product->post->post_excerpt) <=50 ? $product->post->post_excerpt : substr($product->post->post_excerpt, 0,50).'...' ?>
							</span></div>
					</div>
					<div class="row">
						<div class="col-6 d-flex align-items-center"><span class="product-price"><?php echo $product->get_price_html(); ?></span></div>
						<div class="col-6 text-right">
							<?php if($product->is_in_stock()){
							$out_of_stock =''; ?>

							<a rel="nofollow" href="/DelicaBrisbane/?add-to-cart=<?php echo $loop->post->ID; ?>" data-quantity="1" data-product_id="<?php echo $loop->post->ID;?>" data-product_sku="" class="add_to_cart_button ajax_add_to_cart"><span class="fa-stack fa-custom-1 shopping-cart-icon">
								    <i class="fa fa-circle fa-stack-2x"></i>
								    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
								</span></a>
								<?php } else{
								$out_of_stock = 'Out of Stock'; ?>
								<p>
								<span class="fa-stack fa-custom-1 shopping-cart-icon icon-disabled">
								    <i class="fa fa-circle fa-stack-2x"></i>
								    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
								</span></p>
								<?php } ?>
						</div>
						
					</div>
					<div class="row">
						<div class="col"><p>
							<span class="out-of-stock"><?php echo $out_of_stock; ?></span>
						</p></div>
					</div>
				  </div>
				</div>
				              		
					
				
					
						</div>
    				</div>
              </div> 
        <?php    endwhile;
        } else {
            echo __( 'No products found' );
        }
        wp_reset_postdata();
        ?>
		</div>
					<!-- </div> -->
				</div>
				
				<div id="welcome-note" class="section">
					<div class="row">
						<div class="col text-center">
							<?php $welcome = get_field('welcome_note');
								//  $welcome_image = $welcome['image'];

							 ?>
							<h2 class="section-title"><?php echo $welcome['title']  ?></h2>
						</div>
					</div>
					<div class="row">
						<div class="col-md-4"><img src="<?php echo $welcome['image']['url'] ?>" alt=""></div>
						<div class="col-md-8"><?php echo $welcome['description'] ?></div>
					</div>
				</div>

				
				
			</div>
			<div class="col-md-2"><?php get_sidebar( 'right' ); ?></div>
		</div>
	</div>
</section>


<?php get_footer(); ?>