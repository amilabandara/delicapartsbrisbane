<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package understrap
 */

$container = get_theme_mod( 'understrap_container_type' );
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<script type="text/javascript">
function CreateBookmarkLink(){
    var title = document.title;
    var url = window.location.href;
 
    if(window.sidebar && window.sidebar.addPanel){
        /* Mozilla Firefox Bookmark - works with opening in a side panel only � */
        window.sidebar.addPanel(title, url, "");
    }else if(window.opera && window.print) {
        /* Opera Hotlist */
        alert("Press Control + D to bookmark");
        return true;
    }else if(window.external){
        /* IE Favorite */
        try{
            window.external.AddFavorite(url, title);
        }catch(e){
                        alert("Press Control + D to bookmark");
                }            
    }else{
        /* Other */
        alert("Press Control + D to bookmark");
    }
}



</script>

	<?php wp_head(); ?>

	<script>
		

		function explode(){
 
   jQuery('iframe', parent.document).css('width','160px');
 }
 setTimeout(explode, 5000);

jQuery( document ).ready(function($) {
			/*
	Add to cart fly effect with jQuery. - May 05, 2013
	(c) 2013 @ElmahdiMahmoud - fikra-masri.by
	license: https://www.opensource.org/licenses/mit-license.php
*/   

jQuery('.add_to_cart_button').on('click', function () {
        var cart = jQuery('.cart-contents');
        var imgtodrag = jQuery(this).parent().parent().parent().parent('.card').find(".card-img-top");
        console.log(jQuery(this));
        if (imgtodrag) {
            var imgclone = imgtodrag.clone()
                .offset({
                top: imgtodrag.offset().top,
                left: imgtodrag.offset().left
            })
                .css({
                'opacity': '0.5',
                    'position': 'absolute',
                    'height': '150px',
                    'width': '150px',
                    'z-index': '100'
            })
                .appendTo(jQuery('body'))
                .animate({
                'top': cart.offset().top + 10,
                    'left': cart.offset().left + 10,
                    'width': 75,
                    'height': 75
            }, 1000, 'easeInOutExpo');
            
            // setTimeout(function () {
            //     cart.effect("shake", {
            //         times: 2
            //     }, 200);
            // }, 1500);

            imgclone.animate({
                'width': 0,
                    'height': 0
            }, function () {
                jQuery(this).detach()
            });
        }
    });


});
	</script>
</head>

<body <?php body_class(); ?>>

<div class="hfeed site" id="page">

<div id="top-wrapper">
<?php $contacts = get_field('contacts', 'option') ?>
	<div class="top-upper-wrapper container">
		<!-- <div class="container"> -->
			<div class="row d-flex align-items-center">
				<div class="col-md-3 mx-auto text-left">
					<?php the_custom_logo() ?>
				</div>
				<div class="col-md-9 mx-auto text-center">
					<div class="row">
						<div class="col-9">
							<div class="top-menu-wrapper">
								<ul class="list-inline fa-ul">
									<li class="list-inline-item">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><i class="fa-li fa fa-home"></i>Home</a>
									</li>
									<li class="list-inline-item">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>contact-us"><i class="fa-li fa fa-envelope-o"></i>Contact Us</a>
									</li>
									<li class="list-inline-item">
										<a href="<?php echo esc_url( home_url( '/' ) ); ?>site-map"><i class="fa-li fa fa-sitemap"></i>Site Map</a>
									</li>
									<li class="list-inline-item">
										<a href="javascript:CreateBookmarkLink();"><i class="fa-li fa fa-bookmark-o"></i>Bookmark</a>
									</li>
									<li class="list-inline-item"><?php if(is_user_logged_in()){
									echo '<a href="'.wp_logout_url(home_url()).'"><i class="fa-li fa fa-user-circle-o"></i>Sign Out</a>';
								} else {  ?> <a href="<?php echo esc_url( home_url( '/' ) ); ?>my-account"><i class="fa-li fa fa-user-circle-o"></i>Sign in</a><?php } ?>
									
									</li>
								</ul>
							</div>
							
							
						</div>
						
					</div>
					<div class="row">
						<div class="col-9">
							<div class="search-container">
								<?php echo do_shortcode( '[wcas-search-form]' ); ?>
							</div>

					</div>
					<div class="col-3">
							<?php if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	 
							    
							    ?><a class="cart-contents" href="<?php echo WC()->cart->get_cart_url(); ?>" title="<?php _e( 'View your shopping cart' ); ?>">

							    	<?php 
							    if ( $count > 0 ) {
							        ?>
							        <span class="cart-contents-count"><?php echo WC()->cart->get_cart_contents_count(); ?></span>
									<span class="cart-contents-total"><?php echo WC()->cart->get_cart_total(); ?></span>
							        
							        
							        <?php
							    }
							        ?>
							    </a>
							<?php } ?>
						</div>
						</div>
				</div>
				
			</div>
		<!-- </div> -->
	</div>
		
	<div class="top-bar-lower-wrapper container">
		<!-- <div class="container"> -->
			<div class="row d-flex align-items-center">
				<div class="col-md-2"></div>
				<div class="col-md-8 mx-auto text-center">
					<h2><?php //echo get_bloginfo('description'); ?>
<!-- 						THE BEST AND ORIGINAL <span class="small">DELICA PARTS SUPPLIER IN AUSTRALIA</span> -->
						GENUINE AND AFTERMARKET <span class="small">Parts sales in Australia</span>
					</h2>
				</div>
				<div class="col-md-2 text-right">
					<span class="hotline-wrapper"><i class="fa fa-phone" aria-hidden="true"></i> <?php echo $contacts['telephone'] ?></span>
				</div>
			</div>
		<!-- </div> -->
	</div>
		
</div>